﻿using PropertyTools.Wpf;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace CardOnBoard
{
    /// <summary>
    /// Interaction logic for EditableTextBlock.xaml
    /// </summary>
    public partial class EditableTextBlock : UserControl
    {
        private string initialValue;
        private BindingExpression editBinding;
        private bool isEditMode = false;

        public EditableTextBlock()
        {
            InitializeComponent();

            Loaded += EditableTextBlock_Loaded;
        }

        private void EditableTextBlock_Loaded(object sender, RoutedEventArgs e)
        {
            edit.KeyUp += Edit_KeyUp;
            editBinding = edit.GetBindingExpression(TextBox.TextProperty);
        }

        public event EventHandler CellEditionStarted;
        private void Raise_CellEditionStarted()
        {
            CellEditionStarted?.Invoke(this, EventArgs.Empty);
        }

        public event EventHandler CellEditionFinished;
        private void Raise_CellEditionFinished()
        {
            CellEditionFinished?.Invoke(this, EventArgs.Empty);
        }

        public void SwitchMode(bool editModeRequested)
        {
            if (editModeRequested && !isEditMode)
            {
                Raise_CellEditionStarted();
                isEditMode = true;
                Application.Current.MainWindow.MouseDown += MainWindow_PreviewMouseDown;
                StoreInitialValue();

                text.Visibility = Visibility.Collapsed;
                edit.Visibility = Visibility.Visible;
                edit.Focus();
                edit.SelectAll();
            }
            else if (!editModeRequested && isEditMode)
            {
                Application.Current.MainWindow.MouseDown -= MainWindow_PreviewMouseDown;

                editBinding.UpdateSource();
                text.Visibility = Visibility.Visible;
                edit.Visibility = Visibility.Collapsed;
                isEditMode = false;
                Raise_CellEditionFinished();
            }
        }

        private void Edit_KeyUp(object sender, KeyEventArgs e)
        {
            if (e.Key == Key.Enter)
            {
                SwitchMode(false);
            }
            else if (e.Key == Key.Escape)
            {
                SwitchMode(false);
                RestoreInitialValue();
            }
        }

        private void RestoreInitialValue()
        {
            try
            {
                dynamic dataContext = DataContext;
                dataContext.Header = initialValue;
            }
            catch
            {
                // Silent catch
            }
        }

        private void StoreInitialValue()
        {
            try
            {
                dynamic dataContext = DataContext;
                initialValue = dataContext.Header;
            }
            catch
            {
                // Silent catch
            }
        }

        private void UserControl_PreviewMouseDown(object sender, MouseButtonEventArgs e)
        {
            if (e.ClickCount == 2)
            {
                SwitchMode(true);
                // Block the double click from bubbling up.
                // Without this, the MouseDown event reaches the app after this control, and switch the mode to read-only
                e.Handled = true;
            }
        }

        private void MainWindow_PreviewMouseDown(object sender, MouseButtonEventArgs e)
        {
            SwitchMode(false);
        }

        private void Button_Click(object sender, RoutedEventArgs e)
        {
            var button = (Button)sender;
            var contextMenu = new ContextMenu();
            contextMenu.Items.Add(new ColorPickerPanel());
            contextMenu.PlacementTarget = button;
            contextMenu.IsOpen = true;
            contextMenu.Closed += ContextMenu_Closed; ;
        }

        private void ContextMenu_Closed(object sender, RoutedEventArgs e)
        {
            var contextMenu = (ContextMenu)sender;
            Color? selectedColor = ((ColorPickerPanel)contextMenu.Items[0]).SelectedColor;

            // For some reason, when user clicks outside, and does not select a color, 
            // the component returns a color with all properties to 0
            if (selectedColor.HasValue 
                && ! (selectedColor.Value.A == 0 && selectedColor.Value.R == 0 && selectedColor.Value.G == 0 && selectedColor.Value.B == 0))
            {
                ((dynamic)DataContext).Color = HexConverter(selectedColor.Value);
            }
        }

        private static String HexConverter(Color c)
        {
            return "#" + c.R.ToString("X2") + c.G.ToString("X2") + c.B.ToString("X2");
        }
    }
}
