﻿using CardOnBoard.Model;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace CardOnBoard
{
    /// <summary>
    /// Interaction logic for GridDisplay.xaml
    /// </summary>
    public partial class GridDisplay : UserControl
    {
        private ContextMenu gridContextMenu;
        private SelectionCursorBehavior selectionCursorBehavior;

        public GridDisplay()
        {
            InitializeComponent();
            Background = WpfHelper.GetBrush(CardColors.DefaultEmptyColor);

            Loaded += GridDisplay_Loaded;
        }

        private void GridDisplay_Loaded(object sender, RoutedEventArgs e)
        {
            InitContextMenu();
            InitBehaviors();
        }

        #region Properties

        public int NumberOfColumns => grid.ColumnDefinitions.Count;
        public int NumberOfRows => grid.RowDefinitions.Count;

        #endregion

        #region Methods

        public CellDisplay GetCell(int row, int column)
        {
            return grid.Children
                       .Cast<CellDisplay>()
                       .FirstOrDefault(cell => Grid.GetRow(cell) == row 
                                            && Grid.GetColumn(cell) == column);
        }

        public (int, int) GetCellPosition(CellDisplay cell)
        {
            return (Grid.GetRow(cell), Grid.GetColumn(cell));
        }

        private void InitBehaviors()
        {
            selectionCursorBehavior = new SelectionCursorBehavior(this);
            selectionCursorBehavior.RequestAddColumnOrRowInDirection += ButtonAddRowOrColumn_Click;
            selectionCursorBehavior.RequestMoveCellInDirection += SelectionCursorBehavior_RequestMoveCellInDirection;
            selectionCursorBehavior.RequestMoveLineInDirection += SelectionCursorBehavior_RequestMoveLineInDirection;
        }

        public event EventHandler<CardAndDirectionEventArgs> RequestMoveCardInDirection;
        private void Raise_RequestMoveCardInDirection(Card card, EnumDirection direction)
        {
            RequestMoveCardInDirection?.Invoke(this, new CardAndDirectionEventArgs(card, direction));
        }

        public event EventHandler<CardAndDirectionEventArgs> RequestMoveLineInDirection;
        private void Raise_RequestMoveLineInDirection(Card card, EnumDirection direction)
        {
            RequestMoveLineInDirection?.Invoke(this, new CardAndDirectionEventArgs(card, direction));
        }

        private void SelectionCursorBehavior_RequestMoveCellInDirection(object sender, EnumDirection direction)
        {
            var item = (FrameworkElement)sender;
            var card = (Card)item.DataContext;

            Raise_RequestMoveCardInDirection(card, direction);
        }

        private void SelectionCursorBehavior_RequestMoveLineInDirection(object sender, EnumDirection direction)
        {
            var item = (FrameworkElement)sender;
            var card = (Card)item.DataContext;

            Raise_RequestMoveLineInDirection(card, direction);
        }

        #endregion

        #region Context Menu

        private void InitContextMenu()
        {
            gridContextMenu = new ContextMenu();

            var buttonRed = new MenuItem { Header = "Red", Background = WpfHelper.GetBrush(CardColors.Red) };
            var buttonBlue = new MenuItem { Header = "Blue", Background = WpfHelper.GetBrush(CardColors.Blue) };
            var buttonGreen = new MenuItem { Header = "Green", Background = WpfHelper.GetBrush(CardColors.Green) };
            var buttonYellow = new MenuItem { Header = "Yellow", Background = WpfHelper.GetBrush(CardColors.Yellow) };
            var buttonAddLeft = new MenuItem { Header = "Add column to left" };
            var buttonAddTop = new MenuItem { Header = "Add row to top" };
            var buttonAddRight = new MenuItem { Header = "Add column to right" };
            var buttonAddBottom = new MenuItem { Header = "Add row to bottom" };
            var buttonRemoveRow = new MenuItem { Header = "Remove row" };
            var buttonRemoveColumn = new MenuItem { Header = "Remove column" };

            gridContextMenu.Items.Add(buttonRed);
            gridContextMenu.Items.Add(buttonBlue);
            gridContextMenu.Items.Add(buttonGreen);
            gridContextMenu.Items.Add(buttonYellow);
            gridContextMenu.Items.Add(new Separator());
            gridContextMenu.Items.Add(buttonAddLeft);
            gridContextMenu.Items.Add(buttonAddTop);
            gridContextMenu.Items.Add(buttonAddRight);
            gridContextMenu.Items.Add(buttonAddBottom);
            gridContextMenu.Items.Add(new Separator());
            gridContextMenu.Items.Add(buttonRemoveRow);
            gridContextMenu.Items.Add(buttonRemoveColumn);

            buttonRed.Click += (o, _) => ChangeColorTo(o, CardColors.Red);
            buttonBlue.Click += (o, _) => ChangeColorTo(o, CardColors.Blue);
            buttonGreen.Click += (o, _) => ChangeColorTo(o, CardColors.Green);
            buttonYellow.Click += (o, _) => ChangeColorTo(o, CardColors.Yellow);
            buttonAddLeft.Click += (o, _) => ButtonAddRowOrColumn_Click(o, EnumDirection.Left);
            buttonAddTop.Click += (o, _) => ButtonAddRowOrColumn_Click(o, EnumDirection.Up);
            buttonAddRight.Click += (o, _) => ButtonAddRowOrColumn_Click(o, EnumDirection.Right);
            buttonAddBottom.Click += (o, _) => ButtonAddRowOrColumn_Click(o, EnumDirection.Down);
            buttonRemoveRow.Click += (o, _) => ButtonRemoveRow(o);
            buttonRemoveColumn.Click += (o, _) => ButtonRemoveColumn(o);
        }

        private void ChangeColorTo(object sender, string color)
        {
            var item = (FrameworkElement)sender;
            var card = (Card)item.DataContext;

            card.Color = color;
        }

        private void ButtonAddRowOrColumn_Click(object sender, EnumDirection direction)
        {
            var item = (FrameworkElement)sender;
            var card = (Card)item.DataContext;

            Raise_RequestAddColumnOrRowInDirection(card, direction);
        }

        private void ButtonRemoveRow(object sender)
        {
            var item = (FrameworkElement)sender;
            var card = (Card)item.DataContext;

            Raise_RequestRemoveRow(card);
        }

        private void ButtonRemoveColumn(object sender)
        {
            var item = (FrameworkElement)sender;
            var card = (Card)item.DataContext;

            Raise_RequestRemoveColumn(card);
        }

        public event EventHandler<CardAndDirectionEventArgs> RequestAddColumnOrRowInDirection;
        private void Raise_RequestAddColumnOrRowInDirection(Card card, EnumDirection direction)
        {
            RequestAddColumnOrRowInDirection?.Invoke(this, new CardAndDirectionEventArgs(card, direction));
        }

        public event EventHandler<Card> RequestRemoveRow;
        private void Raise_RequestRemoveRow(Card card)
        {
            RequestRemoveRow?.Invoke(this, card);
        }

        public event EventHandler<Card> RequestRemoveColumn;
        private void Raise_RequestRemoveColumn(Card card)
        {
            RequestRemoveColumn?.Invoke(this, card);
        }
       
        private void TextBlock_ContextMenuOpening(object sender, ContextMenuEventArgs e)
        {
            gridContextMenu.PlacementTarget = sender as UIElement;
            gridContextMenu.IsOpen = true;
        }

        #endregion

        #region Drawing grid

        public event EventHandler GridRedrawn;
        private void Raise_GridRedrawn()
        {
            GridRedrawn?.Invoke(this, EventArgs.Empty);
        }

        public event EventHandler CellEditionStarted;
        private void Raise_CellEditionStarted()
        {
            CellEditionStarted?.Invoke(this, EventArgs.Empty);
        }

        public event EventHandler CellEditionFinished;
        private void Raise_CellEditionFinished()
        {
            CellEditionFinished?.Invoke(this, EventArgs.Empty);
        }

        public void RedrawGrid(Card[,] cards)
        {
            Dispatcher.Invoke(() =>
            {
                CleanGrid();

                FillCardsInGrid(cards);

                Raise_GridRedrawn();
            });
        }

        private void CleanGrid()
        {
            grid.ColumnDefinitions.Clear();
            grid.RowDefinitions.Clear();
            grid.Children.Clear();
        }

        private void FillCardsInGrid(Card[,] cards)
        {
            var numberRows = cards.GetLength(0);
            var numberColumns = cards.GetLength(1);

            for (int row = 0; row < numberRows; row++)
            {
                grid.RowDefinitions.Add(new RowDefinition());
            }
            for (int col = 0; col < numberColumns; col++)
            {
                grid.ColumnDefinitions.Add(new ColumnDefinition());
            }

            for (int row = 0; row < numberRows; row++)
            {
                for (int col = 0; col < numberColumns; col++)
                {
                    SetElementInGrid(row, col, CreateElement(cards[row, col]));
                }
            }
        }

        private UIElement CreateElement(Card card)
        {
            var cell = new CellDisplay { DataContext = card };
            cell.ContextMenuOpening += TextBlock_ContextMenuOpening;
            cell.CellEditionStarted += (_, __) => Raise_CellEditionStarted();
            cell.CellEditionFinished += (_, __) => Raise_CellEditionFinished();

            return cell;
        }

        private void SetElementInGrid(int i, int j, UIElement uIElement)
        {
            Grid.SetRow(uIElement, i);
            Grid.SetColumn(uIElement, j);
            grid.Children.Add(uIElement);
        }

        #endregion
    }
}

