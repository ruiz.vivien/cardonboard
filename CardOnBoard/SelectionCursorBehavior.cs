﻿using CardOnBoard.Model;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Input;

namespace CardOnBoard
{
    public class SelectionCursorBehavior
    {
        private readonly GridDisplay grid;
        private int currentRow, currentColumn;
        private EnumMode currentMode = EnumMode.Navigation;

        public SelectionCursorBehavior(GridDisplay grid)
        {
            this.grid = grid;

            App.Current.MainWindow.KeyDown += App_KeyDown;
            grid.GridRedrawn += Grid_GridRedrawn;
            grid.MouseDown += Grid_MouseDown;
            grid.CellEditionStarted += (_, __) => currentMode = EnumMode.CellEdition;
            grid.CellEditionFinished += (_, __) => currentMode = EnumMode.Navigation;

            SetSelectionOnCell(0, 0);
        }

        private void Grid_MouseDown(object sender, System.Windows.Input.MouseButtonEventArgs e)
        {
            var cell = WpfHelper.FindParent<CellDisplay>(e.OriginalSource as DependencyObject);

            if (cell != null)
            {
                var (row, column) = grid.GetCellPosition(cell);
                SetSelectionOnCell(row, column);
            }
        }

        private void Grid_GridRedrawn(object sender, EventArgs e)
        {
            SetSelectionOnCell(currentRow, currentColumn);
        }

        private void App_KeyDown(object sender, System.Windows.Input.KeyEventArgs e)
        {
            switch (currentMode)
            {
                case EnumMode.Navigation:
                    HandleNavigationEvents(e);
                    break;
                case EnumMode.CellEdition:
                    break;
                case EnumMode.GridEdition:
                    HandleGridEditionEvents(e);
                    break;
                default:
                    break;
            }
        }

        public event EventHandler<EnumDirection> RequestAddColumnOrRowInDirection;
        private void Raise_RequestAddColumnOrRowInDirection(CellDisplay cell, EnumDirection direction)
        {
            RequestAddColumnOrRowInDirection?.Invoke(cell, direction);
        }

        public event EventHandler<EnumDirection> RequestMoveCellInDirection;
        private void Raise_RequestMoveCellInDirection(CellDisplay cell, EnumDirection direction)
        {
            RequestMoveCellInDirection?.Invoke(cell, direction);
        }

        public event EventHandler<EnumDirection> RequestMoveLineInDirection;
        private void Raise_RequestMoveLineInDirection(CellDisplay cell, EnumDirection direction)
        {
            RequestMoveLineInDirection?.Invoke(cell, direction);
        }

        private void HandleGridEditionEvents(KeyEventArgs e)
        {
            var currentCell = grid.GetCell(currentRow, currentColumn);

            if (e.Key == Key.Left)
            {
                Raise_RequestAddColumnOrRowInDirection(currentCell, EnumDirection.Left);
            }
            else if (e.Key == Key.Up)
            {
                Raise_RequestAddColumnOrRowInDirection(currentCell, EnumDirection.Up);
            }
            else if (e.Key == Key.Right)
            {
                Raise_RequestAddColumnOrRowInDirection(currentCell, EnumDirection.Right);
            }
            else if (e.Key == Key.Down)
            {
                Raise_RequestAddColumnOrRowInDirection(currentCell, EnumDirection.Down);
            }

            currentCell.SwitchMode(EnumMode.Navigation);
            currentMode = EnumMode.Navigation;
        }

        private void HandleNavigationEvents(KeyEventArgs e)
        {
            if (e.Key == Key.Left || e.Key == Key.Up || e.Key == Key.Right || e.Key == Key.Down)
            {
                var (newRow, newColumn) = (currentRow, currentColumn);

                if (e.Key == Key.Left)
                {
                    newColumn = Math.Max(currentColumn - 1, 0);
                }
                else if (e.Key == Key.Up)
                {
                    newRow = Math.Max(currentRow - 1, 0);
                }
                else if (e.Key == Key.Right)
                {
                    newColumn = Math.Min(currentColumn + 1, grid.NumberOfColumns - 1);
                }
                else if (e.Key == Key.Down)
                {
                    newRow = Math.Min(currentRow + 1, grid.NumberOfRows - 1);
                }

                if (Keyboard.Modifiers == ModifierKeys.Control)
                {
                    var currentCell = grid.GetCell(currentRow, currentColumn);

                    if (e.Key == Key.Left)
                    {
                        Raise_RequestMoveCellInDirection(currentCell, EnumDirection.Left);
                    }
                    else if (e.Key == Key.Up)
                    {
                        Raise_RequestMoveCellInDirection(currentCell, EnumDirection.Up);
                    }
                    else if (e.Key == Key.Right)
                    {
                        Raise_RequestMoveCellInDirection(currentCell, EnumDirection.Right);
                    }
                    else if (e.Key == Key.Down)
                    {
                        Raise_RequestMoveCellInDirection(currentCell, EnumDirection.Down);
                    }
                }

                if (Keyboard.Modifiers == ModifierKeys.Shift)
                {
                    var currentCell = grid.GetCell(currentRow, currentColumn);

                    if (e.Key == Key.Left)
                    {
                        Raise_RequestMoveLineInDirection(currentCell, EnumDirection.Left);
                    }
                    else if (e.Key == Key.Up)
                    {
                        Raise_RequestMoveLineInDirection(currentCell, EnumDirection.Up);
                    }
                    else if (e.Key == Key.Right)
                    {
                        Raise_RequestMoveLineInDirection(currentCell, EnumDirection.Right);
                    }
                    else if (e.Key == Key.Down)
                    {
                        Raise_RequestMoveLineInDirection(currentCell, EnumDirection.Down);
                    }
                }

                SetSelectionOnCell(newRow, newColumn);
                e.Handled = true;
            }
            else if (e.Key == Key.E || e.Key == Key.F2)
            {
                var cell = grid.GetCell(currentRow, currentColumn);
                currentMode = EnumMode.CellEdition;
                cell.SwitchMode(currentMode);
                e.Handled = true;
            }
            else if (e.Key == Key.N)
            {
                var cell = grid.GetCell(currentRow, currentColumn);
                currentMode = EnumMode.GridEdition;
                cell.SwitchMode(currentMode);
                e.Handled = true;
            }
        }

        private void SetSelectionOnCell(int row, int column)
        {
            var currentCell = grid.GetCell(currentRow, currentColumn);

            if (currentCell != null)
            {
                currentCell.SetSelection(false);
            }

            var newCell = grid.GetCell(row, column);

            if (newCell != null)
            {
                newCell.SetSelection(true);

                (currentRow, currentColumn) = (row, column);
            }
            else
            {
                // The cell was not found (for some reason), we try to ensure that we will still have a selected cell.
                if (row == 0 && column == 0)
                {
                    return;
                }

                SetSelectionOnCell(0, 0);
            }
        }
    }
}
