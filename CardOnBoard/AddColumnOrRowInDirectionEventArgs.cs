﻿using CardOnBoard.Model;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CardOnBoard
{
    public class CardAndDirectionEventArgs : EventArgs
    {
        public CardAndDirectionEventArgs(Card card, EnumDirection direction)
        {
            Card = card;
            Direction = direction;
        }

        public Card Card { get; set; }
        public EnumDirection Direction { get; set; }
    }
}
