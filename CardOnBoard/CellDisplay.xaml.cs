﻿using CardOnBoard.Converters;
using CardOnBoard.Model;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace CardOnBoard
{
    /// <summary>
    /// Interaction logic for CellDisplay.xaml
    /// </summary>
    public partial class CellDisplay : UserControl
    {
        public CellDisplay()
        {
            InitializeComponent();
            SetSelection(false);

            editableTextBlock.CellEditionStarted += (_, __) => Raise_CellEditionStarted();
            editableTextBlock.CellEditionFinished += (_, __) => Raise_CellEditionFinished();
        }

        internal void SetSelection(bool selected)
        {
            selectionBorder.BorderBrush = selected ? Brushes.Black : WpfHelper.GetBrush(CardColors.DefaultEmptyColor);
            selectionBorder.CornerRadius = new CornerRadius(0);
        }

        private void SetGridEdition()
        {
            selectionBorder.BorderBrush = Brushes.Blue;
            selectionBorder.CornerRadius = new CornerRadius(40);
        }

        public void SwitchMode(EnumMode mode)
        {
            editableTextBlock.SwitchMode(mode == EnumMode.CellEdition);

            switch (mode)
            {
                case EnumMode.Navigation:
                case EnumMode.CellEdition:
                    SetSelection(true);
                    break;
                case EnumMode.GridEdition:
                    SetGridEdition();
                    break;
                default:
                    break;
            }
        }

        public event EventHandler CellEditionStarted;
        private void Raise_CellEditionStarted()
        {
            CellEditionStarted?.Invoke(this, EventArgs.Empty);
        }

        public event EventHandler CellEditionFinished;
        private void Raise_CellEditionFinished()
        {
            CellEditionFinished?.Invoke(this, EventArgs.Empty);
        }

    }
}
