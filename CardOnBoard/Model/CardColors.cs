﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Media;

namespace CardOnBoard.Model
{
    public static class CardColors
    {
        public const string DefaultEmptyColor = "#FFFFFF";
        public const string DefaultNewCardColor = Blue;

        public const string Yellow = "#F6E16A";
        public const string Red = "#FE898B";
        public const string Blue = "#B0DBEB";
        public const string Green = "#C5EC90";
    }
}
