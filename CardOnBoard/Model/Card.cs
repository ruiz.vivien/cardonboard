﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CardOnBoard.Model
{
    public class Card : BaseModel
    {
        private int? points;
        private string header;
        private string color;
        private int columnPosition;
        private int rowPosition;

        public Card(string header, string color)
        {
            Header = header;
            Color = color;
        }

        public int? Points
        {
            get => points;
            set
            {
                if (points == value)
                    return;

                points = value;
                RaisePropertyChanged();
            }
        }

        public string Header
        {
            get => header;
            set
            {
                if (header == value)
                    return;

                header = value;
                RaisePropertyChanged();
            }
        }

        public string Color
        {
            get => color;
            set
            {
                if (color == value)
                    return;

                color = value;
                RaisePropertyChanged();
            }
        }

        public int ColumnPosition
        {
            get => columnPosition;
            set
            {
                if (columnPosition == value)
                    return;

                columnPosition = value;
                RaisePropertyChanged();
            }
        }

        public int RowPosition
        {
            get => rowPosition;
            set
            {
                if (rowPosition == value)
                    return;

                rowPosition = value;
                RaisePropertyChanged();
            }
        }

        public bool IsEmptyCard => Header == "";
    }
}
