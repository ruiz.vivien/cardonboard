﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Collections.Immutable;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CardOnBoard.Model
{
    public class Board
    {
        #region Properties

        private ImmutableList<Card> cards = ImmutableList<Card>.Empty;
        [JsonProperty]
        public ImmutableList<Card> Cards
        {
            get => cards;
            private set
            {
                if (cards != value)
                {
                    cards = value;

                    RaiseListOfCardsChanged();
                }
            }
        }

        public int NumberOfRows { get; set; }
        public int NumberOfColumns { get; set; }

        #endregion

        #region Constructors

        private Board() { }

        private void InitializeBoard(string dataFile)
        {
            foreach (var card in Cards)
            {
                card.PropertyChanged += Card_PropertyChanged;
            }

            GetGridOfCards();

            CardContentChanged += (_, __) =>
            {
                CalculateColumnPoints();
                PersistBoard(dataFile);
            };
            ListOfCardsChanged += (_, __) => PersistBoard(dataFile);
        }

        public static Board LoadBoard(string dataFile)
        {
            if (!File.Exists(dataFile))
            {
                var emptyBoard = new Board { NumberOfRows = 5, NumberOfColumns = 5 };
                Directory.CreateDirectory(Path.GetDirectoryName(dataFile));
                File.WriteAllText(dataFile, JsonConvert.SerializeObject(emptyBoard));
            }

            var board = JsonConvert.DeserializeObject<Board>(File.ReadAllText(dataFile));
            board.InitializeBoard(dataFile);

            return board;
        }

        #endregion

        #region Public methods

        public void AddColumnOrRowInDirection(Card card, EnumDirection direction)
        {
            switch (direction)
            {
                case EnumDirection.Left:
                    NumberOfColumns++;

                    // OrderByDescending => Allow to start from the column on the most right. This way, we avoid merging column 3 inside column 4, and then moving column 4 cards.
                    foreach (var cardToMove in Cards.Where(c => c.ColumnPosition >= card.ColumnPosition).OrderByDescending(c => c.ColumnPosition))
                    {
                        cardToMove.ColumnPosition++;
                    }
                    break;
                case EnumDirection.Up:
                    NumberOfRows++;

                    foreach (var cardToMove in Cards.Where(c => c.RowPosition >= card.RowPosition).OrderByDescending(r => r.RowPosition))
                    {
                        cardToMove.RowPosition++;
                    }
                    break;
                case EnumDirection.Right:
                    NumberOfColumns++;

                    foreach (var cardToMove in Cards.Where(c => c.ColumnPosition > card.ColumnPosition).OrderByDescending(c => c.ColumnPosition))
                    {
                        cardToMove.ColumnPosition++;
                    }
                    break;
                case EnumDirection.Down:
                    NumberOfRows++;

                    foreach (var cardToMove in Cards.Where(c => c.RowPosition > card.RowPosition).OrderByDescending(r => r.RowPosition))
                    {
                        cardToMove.RowPosition++;
                    }
                    break;
                default:
                    break;
            }

            RaiseListOfCardsChanged();
        }

        internal void MoveLineInDirection(Card card, EnumDirection direction)
        {
            if (card.IsEmptyCard)
            {
                return;
            }

            var (newRow, newColumn) = (card.RowPosition, card.ColumnPosition);
            var (oldRow, oldColumn) = (card.RowPosition, card.ColumnPosition);

            switch (direction)
            {
                case EnumDirection.Left:
                    newColumn = Math.Max(oldColumn - 1, 0);
                    break;
                case EnumDirection.Up:
                    newRow = Math.Max(oldRow - 1, 0);
                    break;
                case EnumDirection.Right:
                    newColumn = Math.Min(oldColumn + 1, NumberOfColumns - 1);
                    break;
                case EnumDirection.Down:
                    newRow = Math.Min(oldRow + 1, NumberOfRows - 1);
                    break;
                default:
                    break;
            }

            if (oldRow != newRow)
            {
                var movingRowCards = Cards.Where(c => c.RowPosition == oldRow).ToList();
                var otherRowCards = Cards.Where(c => c.RowPosition == newRow).ToList();
                foreach (var otherCard in otherRowCards)
                {
                    otherCard.RowPosition = oldRow;
                }

                foreach (var movingCard in movingRowCards)
                {
                    movingCard.RowPosition = newRow;
                }

                RaiseListOfCardsChanged();
            }

            if (oldColumn != newColumn)
            {
                var movingColumnCards = Cards.Where(c => c.ColumnPosition == oldColumn).ToList();
                var otherColumnCards = Cards.Where(c => c.ColumnPosition == newColumn).ToList();
                foreach (var otherCard in otherColumnCards)
                {
                    otherCard.ColumnPosition = oldColumn;
                }

                foreach (var movingCard in movingColumnCards)
                {
                    movingCard.ColumnPosition = newColumn;
                }

                RaiseListOfCardsChanged();
            }
        }

        internal void MoveCardInDirection(Card card, EnumDirection direction)
        {
            if (card.IsEmptyCard)
            {
                return;
            }

            var (newRow, newColumn) = (card.RowPosition, card.ColumnPosition);
            var (oldRow, oldColumn) = (card.RowPosition, card.ColumnPosition);

            switch (direction)
            {
                case EnumDirection.Left:
                    newColumn = Math.Max(oldColumn - 1, 0);
                    break;
                case EnumDirection.Up:
                    newRow = Math.Max(oldRow - 1, 0);
                    break;
                case EnumDirection.Right:
                    newColumn = Math.Min(oldColumn + 1, NumberOfColumns - 1);
                    break;
                case EnumDirection.Down:
                    newRow = Math.Min(oldRow + 1, NumberOfRows - 1);
                    break;
                default:
                    break;
            }

            if (oldRow != newRow || oldColumn != newColumn)
            {
                var otherCard = Cards.FirstOrDefault(c => c.ColumnPosition == newColumn && c.RowPosition == newRow);
                if (otherCard != null)
                {
                    otherCard.RowPosition = oldRow;
                    otherCard.ColumnPosition = oldColumn;
                }

                card.RowPosition = newRow;
                card.ColumnPosition = newColumn;

                RaiseListOfCardsChanged();
            }
        }

        public void RemoveColumn(Card card)
        {
            Cards = Cards.RemoveRange(Cards.Where(c => c.ColumnPosition == card.ColumnPosition).ToList());

            // OrderBy => Allow to start from the column on the most left. This way, we avoid merging column 4 inside column 3, and then moving column 3 cards into column 2 ...
            foreach (var cardToMove in Cards.Where(c => c.ColumnPosition > card.ColumnPosition).OrderBy(c => c.ColumnPosition))
            {
                cardToMove.ColumnPosition--;
            }

            NumberOfColumns--;

            RaiseListOfCardsChanged();
        }

        public void RemoveRow(Card card)
        {
            Cards = Cards.RemoveRange(Cards.Where(c => c.RowPosition == card.RowPosition).ToList());

            // OrderBy => Allow to start from the row on the most left. This way, we avoid merging row 4 inside row 3, and then moving row 3 cards into row 2 ...
            foreach (var cardToMove in Cards.Where(c => c.RowPosition > card.RowPosition).OrderBy(r => r.RowPosition))
            {
                cardToMove.RowPosition--;
            }

            NumberOfRows--;

            RaiseListOfCardsChanged();
        }

        public Card[,] GetGridOfCards()
        {
            var gridOfCards = new Card[NumberOfRows, NumberOfColumns];

            // Place the cards
            foreach (var card in Cards)
            {
                gridOfCards[card.RowPosition, card.ColumnPosition] = card;
            }

            // Fill the rest of the grid with empty cards
            for (int row = 0; row < gridOfCards.GetLength(0); row++)
            {
                for (int col = 0; col < gridOfCards.GetLength(1); col++)
                {
                    if (gridOfCards[row, col] == null)
                    {
                        Card card = GetNewEmptyCard();
                        card.RowPosition = row;
                        card.ColumnPosition = col;

                        gridOfCards[row, col] = card;
                    }
                }
            }

            return gridOfCards;
        }

        #endregion

        private Card GetNewEmptyCard()
        {
            Card card = new Card(string.Empty, CardColors.DefaultEmptyColor);
            card.PropertyChanged += EmptyCard_PropertyChanged;

            return card;
        }

        private void CalculateColumnPoints()
        {
            foreach (var column in Cards.GroupBy(c => c.ColumnPosition))
            {
                var topCard = column.SingleOrDefault(c => c.RowPosition == 0);
                if (topCard != null)
                {
                    topCard.Points = column.Except(new List<Card> { topCard })
                                           .Sum(c => c.Points);
                }
            }
        }

        private void PersistBoard(string dataFile)
        {
            File.WriteAllText(dataFile, JsonConvert.SerializeObject(this, Formatting.Indented));
        }

        private void Card_PropertyChanged(object sender, System.ComponentModel.PropertyChangedEventArgs e)
        {
            var card = (Card)sender;
            if (string.IsNullOrEmpty(card.Header))
            {
                DemoteCardToEmpty(card);
            }

            RaiseCardContentChanged((Card)sender);
        }

        private void EmptyCard_PropertyChanged(object sender, System.ComponentModel.PropertyChangedEventArgs e)
        {
            if (e.PropertyName == nameof(Card.Header))
            {
                PromoteEmptyToCard((Card)sender);
            }
        }

        private void PromoteEmptyToCard(Card card)
        {
            Cards = Cards.Add(card);
            card.PropertyChanged -= EmptyCard_PropertyChanged;
            card.PropertyChanged += Card_PropertyChanged;

            card.Color = CardColors.DefaultNewCardColor;
        }

        private void DemoteCardToEmpty(Card card)
        {
            Cards = Cards.Remove(card);
            card.PropertyChanged += EmptyCard_PropertyChanged;
            card.PropertyChanged -= Card_PropertyChanged;

            card.Header = "";
            card.Color = CardColors.DefaultEmptyColor;
            card.Points = null;
        }

        #region Events

        /// <summary>
        /// A card has been modified
        /// </summary>
        public event EventHandler<Card> CardContentChanged;

        /// <summary>
        /// A card has been added or removed
        /// </summary>
        public event EventHandler ListOfCardsChanged;

        private void RaiseListOfCardsChanged()
        {
            ListOfCardsChanged?.Invoke(this, EventArgs.Empty);
        }

        private void RaiseCardContentChanged(Card card)
        {
            CardContentChanged?.Invoke(this, card);
        }

        #endregion
    }
}
