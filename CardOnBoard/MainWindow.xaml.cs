﻿using CardOnBoard.Model;
using Microsoft.Win32;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Collections.Immutable;
using System.Configuration;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace CardOnBoard
{
    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {
        private Board board;

        public MainWindow()
        {
            InitializeComponent();

            Loaded += MainWindow_Loaded;
            App.Current.MainWindow.KeyDown += MainWindow_KeyDown;
        }

        private void MainWindow_KeyDown(object sender, KeyEventArgs e)
        {
            if (Keyboard.Modifiers == ModifierKeys.Control && e.Key == Key.O)
            {
                var openFileDialog = new OpenFileDialog();
                if (openFileDialog.ShowDialog(this) == true)
                {
                    LoadDataFile(openFileDialog.FileName);
                }
            }
        }

        private void MainWindow_Loaded(object sender, RoutedEventArgs e)
        {
            var dataFile = ConfigurationManager.AppSettings["lastDataFile"];

            gridDisplay.RequestAddColumnOrRowInDirection += (_, args) => GridDisplay_RequestAddColumnOrRowInDirection(args.Card, args.Direction);
            gridDisplay.RequestRemoveRow += GridDisplay_RequestRemoveRow;
            gridDisplay.RequestRemoveColumn += GridDisplay_RequestRemoveColumn;
            gridDisplay.RequestMoveCardInDirection += (_, args) => GridDisplay_RequestMoveCardInDirection(args.Card, args.Direction);
            gridDisplay.RequestMoveLineInDirection += (_, args) => GridDisplay_RequestMoveLineInDirection(args.Card, args.Direction);

            LoadDataFile(dataFile);
        }

        private void LoadDataFile(string dataFile)
        {
            board = Board.LoadBoard(dataFile);
            board.ListOfCardsChanged += (_, __) => Board_ListOfCardsChanged();

            Board_ListOfCardsChanged();
        }

        private void GridDisplay_RequestRemoveColumn(object sender, Card e)
        {
            board.RemoveColumn(e);
        }

        private void GridDisplay_RequestRemoveRow(object sender, Card e)
        {
            board.RemoveRow(e);
        }

        private void GridDisplay_RequestAddColumnOrRowInDirection(Card card, EnumDirection direction)
        {
            board.AddColumnOrRowInDirection(card, direction);
        }

        private void GridDisplay_RequestMoveCardInDirection(Card card, EnumDirection direction)
        {
            board.MoveCardInDirection(card, direction);
        }

        private void GridDisplay_RequestMoveLineInDirection(Card card, EnumDirection direction)
        {
            board.MoveLineInDirection(card, direction);
        }

        private void Board_ListOfCardsChanged()
        {
            gridDisplay.RedrawGrid(board.GetGridOfCards());
        }
    }
}
